const osc = require('osc')

var oscClient = new osc.UDPPort({
  remoteAddress: "127.0.0.1", 
  remotePort: 2020,
  metadata: true
})

oscClient.open()

var x = 0.5
var y = 0.5 
setInterval( () =>{
  var msg = {
    address: "/msg",
    args: [
        {
            type: "s",
            value: ['qond4 varx','holi', 'como estás?', 'chacito', 'ajaja'][Math.floor(Math.random()*4)]
        },
    ]
  }

  var num = {
    address: "/num",
    args: [
        {
            type: "f",
            value: Math.random()
        },
    ]
  }
  

  var xy = {
    address: "/xy",
    args: [
        {
            type: "f",
            value: Math.random()
        },
        {
            type: "f",
            value: Math.random()
        },
    ]
  }

  x = Math.abs((x + ( (Math.random() - 0.5 ) / 10 )) % 1) 
  y = Math.abs((y + ( (Math.random() - 0.5 ) / 10)) % 1) 
  var custom = {
    address: "/custom",
    args:[
      {
        type: 'f', 
        value: x
      },
      {
        type: 'f', 
        value: y
      }
    ]
  }

  console.log('sending data')
  oscClient.send(msg);
  oscClient.send(num);
  oscClient.send(xy);
  oscClient.send(custom)
}, 200)

var routes = ['xy', 'num', 'msg']
var idx = 0

setInterval( () =>{
  var r = {
    address: `/${routes[idx]}/clear`,
    args: [
        {
            type: "f",
            value: Math.random()
        },
    ]
  }
  //oscClient.send(r);
  console.log('reset', routes[idx])
  idx = (idx +  1) % routes.length
}, 10000)
