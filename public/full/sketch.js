const socket = io()

/*
 * SCHEMA
 *
 *    /num/numero --> para series de tiempo
 *    /msg/string --> para texto arbitrario
 *    /xy/[numero,numero] --> para mover un "cursor"
 *    /{custom}/[numero, numero] --> la ruta que se desee
 */

// Serie de tiempo
var timeGraph = p => {

  var vals = []
  var lastVal = 0.5
  var time = 0
  var box = 60

  p.setup = () => {
    p.createCanvas(1240,600)
    p.background('#1c1c1c')
    p.textFont('monospace')
    p.colorMode(p.HSB, p.width)
    socket.on('/num', num => {
      //console.log('num', num)
      lastVal = num
      vals.push(lastVal)
      /*
      time++
      if(time > p.width){
        time = 0
        p.background("#1c1c1c")
      }
      */
    })
    socket.on('/num/clear', () =>{
      p.background('#1c1c1c')
    })
  }

  p.draw = () => {
    var pVal = vals[vals.length - 2]
    //linea
    p.strokeWeight(0.2)
    p.stroke(time, 1240, 1240)
    p.line(time-0.1, p.map(pVal, 0.0, 1.0, p.height - box, 0.0), time, p.map(lastVal, 0.0,1.0, p.height - box, 0) )
    // punto
    p.strokeWeight(2)
    p.stroke('white')
    p.point(time, p.map(lastVal, 0.0, 1.0, p.height - box, 0))
    // texto
    p.fill('#1c1c1c')
    p.noStroke()
    p.rect(0, p.height - box, p.width, p.height)
    p.fill('white')
    p.text('ruta: /num | valor: ' + lastVal, 10, p.height - 10)


    time += 0.1
    if(time > p.width){
      time = 0 
      p.background("#1c1c1c")
    }

  }
}

let tG = new p5(timeGraph)

// Chat
var msgBoard = p => {
  
  var lastMsg = ""
  var time = 0

  p.setup = () => {
    p.createCanvas(600,600)
    p.background('#1c1c1c')
    p.textSize(14)
    p.textFont('monospace')
    p.colorMode(p.HSB, p.height)

    socket.on('/msg', msg => {
      //console.log('new msg!', msg)
      lastMsg = msg
      time+= 20 
      if(time > p.height - 20 ){
        time = 0
        p.background('#1c1c1c')
      }
    })
    socket.on('/msg/clear', () =>{
      p.background('#1c1c1c')
    })
  }

  p.draw = () => {
    //msg
    p.fill(time, 600, 600)
    p.text(lastMsg, 10, time)

    // texto
    p.textSize(12)
    p.fill('#1c1c1c')
    p.noStroke()
    p.rect(0, p.height - 60, p.width, p.height)
    p.fill('white')
    p.text('ruta: /msg | valor: ' + lastMsg, 10, p.height - 10)
  }
}

let mB = new p5(msgBoard)

// Coordenadas X,Y
var drawer = p => {

  var xs = []
  var ys = []
  var lastX  = 0
  var lastY = 0
  var box = 60

  p.setup = () => {

    p.createCanvas(600,600)
    p.background('#1c1c1c')
    p.colorMode(p.HSB, p.width)
    p.strokeWeight(3)

    socket.on('/xy', (coord) => {
      //console.log('x', x)
      lastX = coord[0]//p.map(coord[0], 0, 1, 0, p.width)
      lastY = coord[1]//p.map(coord[1], 0, 1, 0, p.height)
    })

    socket.on('/xy/clear', () =>{
      p.background('#1c1c1c')
    })
    /*
    socket.on('y', y => {
      //console.log('y', y)
      lastY = p.map(y,0,1,0,p.height - 60)
    })
    */
  }

  p.draw = () => {
    p.stroke(lastX * p.width , 600,600)
    p.point(lastX * (p.width - box),p.map (lastY, 0.0,1.0, p.height - box , 0.0))

    // info
    p.fill('#1c1c1c')
    p.noStroke()
    p.rect(0, p.height - 60, p.width, p.height)
    p.fill('white')
    p.text('ruta: /xy | x: ' + lastX + ' y: ' + lastY , 10, p.height - 10)

  }
}

let dw = new p5(drawer)

var custom = p =>{
  var input
  var button
  var route = ""
  var prevRuta = ""
  var valor = [0.5,0.5]
  var vals = []
  var box = 80


  p.setup = () => {
    canvas = p.createCanvas(1240,600)
    p.colorMode(p.HSB, p.width)

    input = p.createInput('ruta') 
    button = p.createButton('ver')
    input.parent('#app')
    button.parent('#app')
    input.position(32, -p.height - 10, 'relative')
    button.position(32, -p.height - 10, 'relative')
    button.mousePressed(onAceptarRuta);
    input.input(onInput)

  }

  function onInput(){
    route = this.value()
  }

  function onAceptarRuta(){

    if(prevRuta !== ""){
      //socket
      socket.off(prevRuta)
    }

    socket.on(route, (val) =>{
      valor = val
      vals.push(valor)
    })

    socket.on(`${route}/clear`, () =>{
      p.background('#1c1c1c')
      valor = [0.5, 0.5]
      vals = []

    })

    prevRuta = route

  }

  p.draw = () =>{
    //console.log(p.mouseX, p.mouseY);
    if(vals.length > 2){
      var pVal = vals[vals.length - 2]
      var d = p.dist(valor[0] * (p.width - box), valor[1] * (p.height - box), p.width/2, p.height/2)
      p.stroke(d, 1240,1240)
      p.line(
        pVal[0] * (p.width ),
        p.map(pVal[1], 0.0, 1.0, p.height, 0.0), 
        valor[0] * (p.width),
        p.map(valor[1], 0.0, 1.0, p.height, 0)
      )
    }
    // info
    p.fill('#1c1c1c')
    p.noStroke()
    p.rect(0, p.height - 60, p.width, p.height)
    p.fill('white')
    p.text('ruta: ' + route + ' | ' + valor, 10, p.height - 10)
    /*
    p.fill('white')
    p.text(valor, p.width/2, p.height/2)
    */
  }
}

let c = new p5(custom)



