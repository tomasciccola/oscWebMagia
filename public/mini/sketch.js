const socket = io()

/*
 * SCHEMA
 *
 *    /num/numero --> para series de tiempo
 *    /msg/string --> para texto arbitrario
 *    /xy/[numero,numero] --> para mover un "cursor"
 *    /{custom}/[numero, numero] --> la ruta que se desee
 */

var canvas

var modes = ["xy", "serie de tiempo", "texto"]
var modeSelected = ""
var ruta = ""
var prevRuta = ""

var vals = []
var lastVal

var pad = 60
var tSize = 14

time = 0

//UI
var modeSelector, routeInput, aceptar

function setup(){
  canvas = createCanvas(600,600)

  canvas.parent('#container')
  colorMode(HSB, width)
  background('#1c1c1c')


  textSize(tSize)
  textFont('monospace')

  modeSelector = createSelect()

  for(mode of modes){
    modeSelector.option(mode)
    modeSelector.class('.selector')
  }

  routeInput = createInput('ruta')

  aceptar = createButton('aceptar')

  modeSelector.parent('#ui')
  routeInput.parent('#ui')
  aceptar.parent('#ui')

  aceptar.mousePressed(onAceptar)

}

function onInputRuta(){
  ruta = this.value()
}


function onAceptar(){

  modeSelected = modeSelector.value()
  ruta = routeInput.value()

  if(prevRuta !== ""){
    socket.off(prevRuta)
  }

  reset()

  if(ruta.indexOf('/') === -1){
    ruta = `/${ruta}`
  }

  console.log(`seleccionado modo ${modeSelected}  y la ruta es: ${ruta}`)

  socket.on(ruta, (val) =>{

    console.log(ruta, val)

    lastVal = val
    vals.push(val)

    //update
    time ++

    if(modeSelected !== 'texto'){
      if(time > width){
        reset()
      }
    }else{
      if(time * tSize > width){
        reset()
      }
    }
  })


  socket.on(`${ruta}/clear`, reset)

  prevRuta = ruta
}

function reset(){
  time = 0
  vals = []
  background('#1c1c1c')
}

function draw(){
  switch(modeSelected){
    case 'xy':
      drawXY()
      break
    case 'serie de tiempo':
      drawTimeSeries()
      break
    case 'texto':
      drawText()
      break
  }
  drawInfo()
}

function drawXY(){
  strokeWeight(3)
  if(vals.length > 0){
    stroke(vals[vals.length - 1][0] * width, width, width)
    var lastX = vals[vals.length - 1][0]
    var lastY = vals[vals.length - 1][1]
    point(map(lastX,0.0,1.0, pad, width - pad), map(lastY, 0.0,1.0, height - pad, pad))
  }



}

function drawTimeSeries(){
  //línea
  strokeWeight(0.4)
  stroke(time, width, width)
  line(
    time - 0.1,
    map(
      vals[vals.length - 2],
      0.0,
      1.0,
      height - pad,
      pad
    ),
    time,
    map(
      vals[vals.length - 1],
      0.0,
      1.0,
      height - pad,
      pad
    )
  )

  //punto
  strokeWeight(2)
  stroke('white')
  point(time, map(vals[vals.length - 1], 0.0,1.0, height - pad, pad))


}

function drawText(){
  fill(time, width, width)
  if(vals.length > 1){
    text(vals[vals.length - 1][0], 10,  time * tSize)
  }

}

function drawInfo(){
  fill('#1c1c1c')
  noStroke()
  rect(0,height - pad, width, height)
  fill('white')
  text(`ruta: ${ruta} | último valor: ${vals[vals.length - 1] || 'nada'}`, 10, height - 10)
}
