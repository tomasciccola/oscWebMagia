const fs = require('fs')
const http = require('http')
const path = require('path')
const osc = require('osc')

//const WEB_PORT = 80
const OSC_PORT = 2020

const ROOT = 'public/'
const FULL = 'full'
const MINI = 'mini'

const indexMini = fs.readFileSync(path.join(ROOT,MINI, 'index.html')).toString()
const jsMini = fs.readFileSync(path.join(ROOT,MINI,'sketch.js')).toString()

const indexFull = fs.readFileSync(`${ROOT}${FULL}/index.html`).toString()
const jsFull = fs.readFileSync(`${ROOT}${FULL}/sketch.js`).toString()


//WEB
var server = http.createServer( (req,res) => {
  console.log('req', req.url)
  //MINI --> "/"
  if(req.url === '/'){
    res.end(indexMini) 
  }else if(req.url === '/sketch.js'){
    res.end(jsMini) 
  // FULL --> "/full"
  }else if(req.url === '/full'){
    res.end(indexFull)
  }else if(req.url === '/full/sketch.js'){
    res.end(jsFull)
  }else{
    console.log(req.url)
    console.log('ni idea che...')
    res.end('mmm, me pa que le pifiaste de ruta...')
  }
})

server.listen(process.env.PORT || 80, () =>{
  console.log(`web server listening on ${process.env.PORT || 80}`)
})

//WEBSOCKET
const io = require('socket.io')(server)

io.on('connection', client => {
  console.log('new client!')
})


// OSC
var oscServer = new osc.UDPPort({
  localAddress: "0.0.0.0",
  localPort: OSC_PORT
})


oscServer.on('ready', address => {
  console.log(`osc server listening on ${OSC_PORT}`)
})

oscServer.on('message', message =>{
  var { address: route, args : val } = message
  if(route.indexOf('clear') !== -1){
    console.log(route, val)
  }
  //console.log(route, val)
  io.sockets.emit(route, val)
})

oscServer.on('error', message =>{
  console.log(`oops! error --> ${message}`)
})

oscServer.open()


