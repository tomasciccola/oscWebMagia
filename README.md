# Visualizador mágico de información OSC

Básicamente un server web que recibe data vía osc y la manda a la web vía websocket.

Hay algunas rutas y puertos predefinidas, así también maneras de ver la data


**OJO: por defecto esto corre en el puerto 80!! (de ahí lo mágico)**
Si lo corres así nomás, correlo con sudo. Si no querés que sea accesible públicamente cambia WEB_PORT por otro puerto

para correr, instalar nodejs y usar:

```
node server.js
```

Además hay un cliente para testear los mensajes (oscSender.js)
